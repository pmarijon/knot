# About dataset

This is a 20x synthetic dataset created by LongISLND, with PacBio error model, on *Terriglobus roseus* (NC_108014.1).

Assembled with canu 1.7.1 with parameter `genomeSize=5m --corOutCoverage=20`.

# File description

- assembly_contig.fasta: contigs, generated by canu
- assembly_contig.gfa: contig graph, generated by canu
- reads_raw.fasta: simulated reads generated by LongISLND
- reads_corrected.fasta: reads corrected by canu

# How to run knot

We assume knot is available  in your path and you are in demo folder.

On raw reads:

```
knot -r reads_raw.fasta -c assembly_contig.fasta -g assembly_graph.gfa -o raw
```

On corrected reads:

```
knot -C reads_corrected.fasta -c assembly_contig.fasta -g assembly_graph.gfa -o corrected
```
